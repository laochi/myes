<?php

use think\facade\Route;

if (! defined('CNS_V2_0_1')) return;

Route::group('/api/v2_0_1.search', function() {

    Route::group('/goods', function() {//商品搜索
        Route::post('common', CNS_V2_0_1. 'SearchGoods@common');//综合搜索
        Route::post('namesuggest', CNS_V2_0_1. 'SearchGoods@nameSuggest');//商品名称补全
    });
    // ->middleware([
    //     \app\middleware\CheckUserAuth::class,
    // ]);

})->miss(CNS_V2_0_1. 'SearchGoods@miss');
