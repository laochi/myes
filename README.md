ElasticSearch搜索服务

针对 dy_goods 表 id, name, keyword, descword 字段, 联合 dy_goods_category 表 (dy_goods.c_id=dy_goods_cateogry.c_id) 的 c_id, name 建立索引

提供针对商品名称(name)及其拼音的补全服务 (nameSuggest), 和重点针对商品关键词(keyword)字段的综合搜索的服务 (common)

综合搜索对于同时存在拼音和汉字的文字,汉字权重更高

通过thinkphp命令行提供索引基本管理,包括建立索引结构,删除索引结构,导入索引文档,

可以进行手动和自动全量更新索引数据.

[多版本控制]

现在支持多版本控制器和多版本输出模板控制.需要在 route/ 目录中添加新版本路由文件指定到新的控制器.

service, model, entity, command 不受控制.

[数据同步]

执行命令行 php think es index:refresh goods 可以自动查找需同步的数据 (is_synchro=1) ,同步后将置为已同步.





[前台接口]

商品名称或拼音补全

/search/goods/namesuggest?search_text=初cai竹

商品综合

/search/goods/common?search_text=补水

[命令行管理]

查看帮助信息

php think es help

查看ES服务信息

php think es server:info

创建商品索引 

php think es index:create goods

删除商品索引

php think es index:delete goods

导入商品索引

php think es index:write goods

查看索引是否创建

php think es index:exists goods

刷新索引数据

php think es index:refresh goods

重建索引数据

php think es index:rebuild goods




[开发环境.ENV文件]

APP_DEBUG = true

[APP]

DEFAULT_TIMEZONE = Asia/Shanghai

[DATABASE]

HOSTNAME = 

DATABASE = dayu

USERNAME = uroot

PASSWORD = Kcd_5556

HOSTPORT = 61547



[REDIS1]

IP1 = 10.0.0.2

PORT = 6379

PWD = chengA6shits

[TENCENT_ES]

HOST = ""

PORT = "9200"

SCHEME = "https"

USERNAME = "elastic"

PASSWORD = "Dayu_es_2"