<?php
namespace factory;

use think\facade\Env;

class Redis
{
    private static $_instance;
    public static function getInstance()
    {
        if (self::$_instance) {
            return self::$_instance;
        }
        $redis = new \Redis();
        $redis->connect(Env::get('REDIS1.IP1'), Env::get('REDIS1.PORT'));
        if (Env::get('REDIS1.PWD')) {
            $redis->auth(Env::get('REDIS1.PWD'));
        }
        return self::$_instance = $redis;
    }
}