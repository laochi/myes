<?php
namespace app\outputs\v0_1\output\structs\goods\common;

use app\outputs\v0_1\output\structs\interfaces\DetailInterface;
use app\outputs\v0_1\output\builders\goods\common\Builder;

class Detail implements DetailInterface
{
    public $total = 0;
    public $is_more = false;
    public $list = [];

    function __construct(array $data)
    {
        if (isset($data['total'])) {
            $this->total = $data['total'];
        }
        if (isset($data['list']) && $data['list']) {
            $this->list = Builder::buildList($data['list']);
        }
        if (($pageNo = $data['pageno'] ?? null) && ($pageSize = $data['pagesize'] ?? null) && ($total = $data['total'] ?? null)) {
            $dataEndNo = $pageNo * $pageSize;
            if ($dataEndNo < $total) {
                $this->is_more = true;
            }
        }
    }
}