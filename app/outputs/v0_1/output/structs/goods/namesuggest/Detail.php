<?php
namespace app\outputs\v0_1\output\structs\goods\namesuggest;

use app\outputs\v0_1\output\structs\interfaces\DetailInterface;
use app\outputs\v0_1\output\builders\goods\namesuggest\Builder;

class Detail implements DetailInterface
{
    public $list = [];

    function __construct(array $data)
    {
        if (isset($data['list']) && $data['list']) {
            $this->list = Builder::buildList($data['list']);
        }
    }
}