<?php
namespace app\outputs\v0_1\output\builders;

use think\model\Collection;
use app\outputs\v0_1\output\structs\interfaces\ListItemInterface;
use app\outputs\v0_1\output\structs\interfaces\DetailInterface;
use app\entity\EsEntityInterface;

abstract class BaseBuilder
{
    public static function buildList($data)
    {
        if (is_array($data)) {
            return self::buildListFromArray($data);
        } else if ($data instanceof Collection) {
            return self::buildListFromCollection($data);
        }
        return [];
    }

    public static function buildDetail(array $data)
    {
        return self::buildDetailFromArray($data);
    }

    public static function buildListFromArray(array $arr)
    {
        $list = [];
        foreach ($arr as $item) {
            $list[] = self::createListItem($item);
        }
        return $list;
    }

    public static function buildListFromCollection(Collection $collection)
    {
        $list = [];
        foreach ($collection as $item) {
            $list[] = self::createListItem($item);
        }
        return $list;
    }

    public static function createListItem($item) : ListItemInterface
    {
        $itemClass = static::getListItem();
        return new $itemClass($item);
    }

    public static function buildDetailFromArray(array $arr)
    {
        return self::createDetail($arr);
    }

    public static function createDetail($data) : DetailInterface
    {
        $detailClass = static::getDetail();
        return new $detailClass($data);
    }
}