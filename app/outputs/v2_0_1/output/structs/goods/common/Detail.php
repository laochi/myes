<?php
namespace app\outputs\v2_0_1\output\structs\goods\common;

use app\outputs\v2_0_1\output\structs\interfaces\DetailInterface;
use app\outputs\v2_0_1\output\builders\goods\common\Builder;

class Detail implements DetailInterface
{
    public $total = 0;
    public $is_more = false;
    public $is_more_int = 0;
    public $list = [];

    function __construct(array $data)
    {
        if (isset($data['total'])) {
            $this->total = $data['total'];
        }
        if (isset($data['list']) && $data['list']) {
            $this->list = Builder::buildList($data['list']);
            // $this->list = [];
            // foreach ($objList as $k => $item) {
            //     $this->list[$k] = get_object_vars($item);
            // }
            // if ($sort = $data['sort']) {
            //     $sortFieldArr = array_column($this->list, $sort['field']);
            //     if ($sort['order'] == 'asc') {
            //         $sortType = SORT_ASC;
            //     } else {
            //         $sortType = SORT_DESC;
            //     }
            //     array_multisort($sortFieldArr, $sortType, $this->list);
            // }
        }
        if (($pageNo = $data['pageno'] ?? null) && ($pageSize = $data['pagesize'] ?? null) && ($total = $data['total'] ?? null)) {
            $dataEndNo = $pageNo * $pageSize;
            if ($dataEndNo < $total) {
                $this->is_more = true;
                $this->is_more_int = 1;
            }
        }
        if ($data['debug'] ?? null) {
            $this->debug = $data['debug'];
        }
    }
}