<?php
namespace app\outputs\v2_0_2\output\builders\goods\common;

use app\outputs\v2_0_2\output\builders\BaseBuilder;
use app\outputs\v2_0_2\output\structs\goods\common\ListItem;
use app\outputs\v2_0_2\output\structs\goods\common\Detail;

class Builder extends BaseBuilder
{
    private const LIST_ITEM = ListItem::class;
    private const DETAIL = Detail::class;

    protected static function getListItem() : string
    {
        return self::LIST_ITEM;
    }

    protected static function getDetail() : string
    {
        return self::DETAIL;
    }
}