<?php
namespace app\outputs\v2_0_2\output\structs\goods\namesuggest;

use app\outputs\v2_0_2\output\structs\interfaces\DetailInterface;
use app\outputs\v2_0_2\output\builders\goods\namesuggest\Builder;

class Detail implements DetailInterface
{
    public $list = [];

    function __construct(array $data)
    {
        $this->list = Builder::buildList($data);
    }
}