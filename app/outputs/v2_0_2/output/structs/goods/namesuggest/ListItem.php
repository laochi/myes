<?php
namespace app\outputs\v2_0_2\output\structs\goods\namesuggest;

use app\outputs\v2_0_2\output\structs\interfaces\ListItemInterface;
use app\entity\GoodsEsEntity;

class ListItem implements ListItemInterface
{
    public $g_id = 0;
    public $name = '';
    public $keyword = '';
    public $descword = '';
    public $category = [];

    function __construct(GoodsEsEntity $esEntity)
    {
        $this->g_id = $esEntity->getGId();
        $this->name = $esEntity->getName();
        $this->keyword = $esEntity->getKeyword();
        $this->category = $this->assignCategory($esEntity);
    }

    public static function assignCategory(GoodsEsEntity $esEntity)
    {
        $category = [
            'level1' => [
                'c_id' => $esEntity->getCategoryIdLv1(),
                'name' => $esEntity->getCategoryNameLv1(),
            ],
            'level2' => [
                'c_id' => $esEntity->getCategoryIdLv2(),
                'name' => $esEntity->getCategoryNameLv2(),
            ],
            'level3' => [
                'c_id' => $esEntity->getCategoryIdLv3(),
                'name' => $esEntity->getCategoryNameLv3(),
            ],
        ];
        return $category;
    }
}