<?php
namespace app\outputs\v2_0_2\output\structs\goods\common;

use think\facade\Db;
use app\outputs\v2_0_2\output\structs\interfaces\ListItemInterface;
use app\entity\GoodsEsEntity;
use app\model\GoodsModel;
use app\model\GoodsPicModel;
use app\model\PartnerModel;
use app\model\PreDividendTodayModel;
use factory\Redis as RedisFactory;
use app\constants\Tencent as TencentConst;
use app\services\UserAuthService;

class ListItem implements ListItemInterface
{
    public $s_id = 0;
    public $g_id = '';
    public $logo = '';
    public $dividend_ratio = '0.0';
    public $weight = '0.0';
    public $name = '';
    public $partner = 0;
    public $current_price = '0.0';
    public $region_name = '';
    public $payment_sum = 0;
    public $release = 0;
    public $votes = 0;
    public $postage = '';
    public $lable = [];
    public $coupon = '';
    public $stockholder_val = [];
    public $partner_val = [];
    public $isagent = false;
    public $stockholder_max = 0;
    public $vote = false;
    public $vote_sum = 0;
    public $keyword = '';
    public $descword = '';
    public $category = [];
    public $region_id = 0;
    public $new_goods_delivery_time = '';
    public $profit_week_scale = '';
    public $shelf_time = 0;
    public $sales_actual = 0;
    public $chip = 0;
    public $status = 0;
    public $c_id = 0;
    public $thisweek_about_bonus = '0.0';

    function __construct(GoodsEsEntity $esEntity)
    {
        $this->g_id = (string) $esEntity->getGId();
        $this->name = $esEntity->getName();
        $this->keyword = $esEntity->getKeyword();
        $this->category = $this->assignCategory($esEntity);
        $this->s_id = $esEntity->getSId();
        $this->partner = $esEntity->getPartner();
        $this->current_price = (string) $this->currentPrice($esEntity);
        $this->region_name = $esEntity->getRegionName();
        $this->payment_sum = $esEntity->getPaymentSum();
        $this->release = $esEntity->getRelease();
        $this->votes = $esEntity->getVotes();
        $this->postage = $esEntity->getPostage();
        // $this->dividend_ratio = $esEntity->getDividendRatio();
        $this->weight = (string) $esEntity->getWeight();
        $this->logo = $this->getGoodsLogo($esEntity);
        $this->partner_val = $this->getPartnerImg($esEntity);
        $this->stockholder_val = $this->getStockholderImg($esEntity);
        $this->isagent = $this->isAgent($esEntity);
        $this->vote = $this->getVoteInfo($esEntity);
        $this->stockholder_max = $this->getStockholderMax();
        $this->vote_sum = $this->getVoteSum();
        $this->partner = $this->calculatePartner($esEntity);
        $this->dividend_ratio = $this->getDividendRatio($esEntity);
        $this->region_id = $esEntity->getRegionId();
        $this->new_goods_delivery_time = $esEntity->getNewGoodsDeliveryTime();
        $this->profit_week_scale = $esEntity->getProfitWeekScale();
        $this->shelf_time = $esEntity->getShelfTime();
        $this->sales_actual = $esEntity->getSalesActual();
        $this->chip = $esEntity->getChip();
        $this->status = $esEntity->getStatus();
        $this->c_id = $esEntity->getCId();
        $this->thisweek_about_bonus = $this->getThisweekAboutBonus($esEntity);
    }

    private static function getThisweekAboutBonus(GoodsEsEntity $esEntity) {
        $datetime = date('Ymd', time() - 86400);
        $gid = $esEntity->getGId();
        $thisweek_about_bonus = 0.00;
        $today_jin = Db::name('dy_pre_dividend_today')
            ->where([['g_id', '=', $gid]])
            ->where([['type', 'in', [3, 4]]])
            ->where([['createdate', '=', $datetime]])
            ->field('sum(pre_today_money) as pre_today_money')
            ->select();
        if(!empty($today_jin) && $today_jin[0]['pre_today_money'] != null  && $today_jin[0]['pre_today_money']!=''){
            $thisweek_about_bonus = $today_jin[0]['pre_today_money'];
        }
        // if ($esEntity->getPreTodayMoneyDate() == $datetime) {
        //     return (string) $esEntity->getPreTodayMoney();
        // }
        return $thisweek_about_bonus;
    }

    private static function currentPrice(GoodsEsEntity $esEntity)
    {
        return sprintf('%0.2f', $esEntity->getCurrentPrice());
    }

    public static function getDividendRatio(GoodsEsEntity $esEntity) : string
    {
        $weight = $esEntity->getWeight();
        $dividendRatio = $esEntity->getDividendRatio();
        return (string) ($weight > 0) ? $weight : bcmul($dividendRatio, 61.8, 2);
    }


    private static function calculatePartner(GoodsEsEntity $esEntity) : string
    {
        $goodsId = $esEntity->getGId();
        $partnerMax = $esEntity->getPartnerMax();
        $partnerCount = PartnerModel::getGoodsPartnerCount($goodsId);
        $partner = $partnerCount. '/'. $partnerMax;
        return $partner;
    }

    private static function getVoteSum() : int
    {
        return 3;
    }

    private static function getStockholderMax() : int
    {
        return 3;
    }

    private static function getVoteInfo(GoodsEsEntity $esEntity) : bool
    {
        $redis = RedisFactory::getInstance();
        $goodsId = $esEntity->getGId();
        $userId = UserAuthService::getUserId();
        return (bool) $redis->sismember('toupiao:' . $goodsId, $userId);
    }

    private static function isAgent(GoodsEsEntity $esEntity) : bool
    {
        $goodsId = $esEntity->getGId();
        $userId = UserAuthService::getUserId();
        return (bool) PartnerModel::isUserGoodsAgent($goodsId, $userId);
    }

    private static function getStockholderImg(GoodsEsEntity $esEntity) : array
    {
        $redis = RedisFactory::getInstance();
        $goodsId = $esEntity->getGId();
        $overdueTime = GoodsModel::getGoodsStockHolderTime($goodsId);
        $imgArr = [];
        if ($overdueTime > time()) {
            $shareholder = json_decode($redis->hget('shareholder', $goodsId), true);
            if ($shareholder) {
                foreach ($shareholder as $ki => $vi) {
                    $imgArr[$ki] = $vi['avatar'];
                }
            } else {
                $imgArr = [TencentConst::PIC['pic_location']. 'demo/20200622/202006221610529910051.png'];
            }
        }
        return $imgArr;
    }

    private static function getPartnerImg(GoodsEsEntity $esEntity) : array
    {
        $redis = RedisFactory::getInstance();
        $goodsId = $esEntity->getGId();
        $imgArr = [];
        $partner_val = json_decode($redis->hget('partner', $goodsId), true);
        if ($partner_val) {
            foreach ($partner_val as $k => $v) {
                $imgArr[] = $v['avatar'];
            }
        } else {
            $imgArr = [TencentConst::PIC['pic_location']. 'demo/20200622/202006221610529910051.png'];
        }
        return $imgArr;
    }

    private static function getGoodsLogo(GoodsEsEntity $esEntity) : string
    {
        $goodsId = $esEntity->getGId();
        return (string) GoodsPicModel::getGoodsPic($goodsId);
    }

    private static function assignCategory(GoodsEsEntity $esEntity)
    {
        $category = [
            'level1' => [
                'c_id' => $esEntity->getCategoryIdLv1(),
                'name' => $esEntity->getCategoryNameLv1(),
            ],
            'level2' => [
                'c_id' => $esEntity->getCategoryIdLv2(),
                'name' => $esEntity->getCategoryNameLv2(),
            ],
            'level3' => [
                'c_id' => $esEntity->getCategoryIdLv3(),
                'name' => $esEntity->getCategoryNameLv3(),
            ],
        ];
        return $category;
    }
}