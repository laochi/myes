<?php
declare (strict_types = 1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Facade\Env;
use app\services\ElasticSearch\Constants as EsConstants;
use app\services\ElasticSearchService;
use app\model\PreDividendTodayModel;

class ElasticSearch extends Command
{
    private static $logoutput;

    protected function configure()
    {
        $this->setName('ElasticSearch')
            ->setDescription("ElasticSearch索引管理")
            ->addArgument('actionName', Argument::OPTIONAL, '操作名称')
            ->addArgument('indexName', Argument::OPTIONAL, '索引名称');
    }

    protected function execute(Input $input, Output $output)
    {
        $actionName = $input->getArgument('actionName') ?: '';
        $indexName = $input->getArgument('indexName') ?: '';
        switch ($actionName) {
            case 'help':
                $this->help();
            break;
            case 'server:info':
                $this->serverInfo();
            break;
            case 'index:create':
                $this->indexCreate($indexName);
            break;
            case 'index:write':
                $this->indexWrite($indexName);
            break;
            case 'index:delete':
                $this->indexDelete($indexName);
            break;
            case 'index:exists':
                $this->indexExists($indexName);
            break;
            case 'index:refresh':
                $this->indexRefresh($indexName);
            break;
            case 'index:rebuild':
                $this->indexDelete($indexName);
                $this->indexCreate($indexName);
                $this->indexWrite($indexName);
            break;
            default:
                $this->help();
            break;
        }
    }

    protected function help()
    {
        $this->wln("ElasticSearch索引管理");
        $this->wln("  help\t---\t帮助信息");
        $this->wln("  server:info\t---\tES服务信息");
        $this->wln("  index:exists {index_name}\t---\t索引存在否");
        $this->wln("  index:create {index_name}\t---\t创建索引");
        $this->wln("  index:write {index_name}\t---\t索引数据");
        $this->wln("  index:delete {index_name}\t---\t删除索引");
        $this->wln("  index:refresh {index_name}\t---\t刷新索引");
        $this->wln("  index:rebuild {index_name}\t---\t重建索引");
        $this->wln("例如 php think es index:create goods 或 php think es server:info\n");
    }

    protected function serverInfo()
    {
        list($resp, $errCode, $errMsg) = ElasticSearchService::serverInfo();
        if ($errMsg != 'ok' || $errCode != 200) {
            return $this->wln("SERVER INFO FAILED\nReason: \n". $errMsg);
        }
        $this->wln('SERVER INFO');
        foreach ($resp as $key => $row) {
            $this->wln($key. ': '. $row);
        }
    }

    protected function indexCreate(string $indexName)
    {
        if (! $indexName) {
            return $this->wln("Index name Required");
        }
        $params = [
            'index_name' => $indexName,
        ];
        list($resp, $errCode, $errMsg) = ElasticSearchService::indexExists($params);
        if ($errMsg != 'ok' || $errCode != 200) {
            return $this->wln("INDEX EXISTS DETECT FAILED\nReason: \n". $errMsg);
        }
        if (isset($resp['exists']) && $resp['exists']) {
            return $this->wln('Index "'. $indexName. '" ALREADY EXISTS');
        }
        $json = EsConstants::INDEX_CREATE_JSON_MAP[$indexName] ?? '';
        if (! $json) {
            return $this->wln("Index create json not found");
        }
        $params = [
            'index_name' => $indexName,
            'create_json' => $json,
        ];
        list($resp, $errCode, $errMsg) = ElasticSearchService::indexCreate($indexName, $params);
        if ($errMsg != 'ok' || $errCode != 200) {
            return $this->wln("CREATE INDEX '". $indexName. "' FAILED\nReason: \n". $errMsg);
        }
        if (isset($resp['success']) && isset($resp['index'])) {
            return $this->wln('Create Index "'. $resp['index']. '" SUCCESS');
        }
        return $this->wln('CREATE FAILED');
    }

    protected function indexWrite(string $indexName)
    {
        if (! $indexName) {
            return $this->wln("Index name Required");
        }
        $params = [
            'index_name' => $indexName,
        ];
        list($resp, $errCode, $errMsg) = ElasticSearchService::indexExists($params);
        if ($errMsg != 'ok' || $errCode != 200) {
            return $this->wln("INDEX EXISTS DETECT FAILED\nReason: \n". $errMsg);
        }
        if (isset($resp['exists']) && ! $resp['exists']) {
            return $this->wln('Index "'. $indexName. '" NOT EXISTS');
        }
        $modelName = EsConstants::MODEL_MAP[$indexName] ?? '';
        if (! $modelName) {
            return $this->wln('Index data model not found');
        }
        $esEntityName = EsConstants::ES_ENTITY_MAP[$indexName] ?? '';
        if (! $esEntityName) {
            return $this->wln('Index data entity not found');
        }
        $dataCount = $modelName::getEsIndexDataCount();
        if (! $dataCount) {
            return $this->wln("No data to index");
        }
        $this->wln('DataCount:');
        $this->wln($dataCount);
        $this->wln('Please wait');
        $batchCount = 100;
        $successCount = 0;
        $page = 1;
        for ($i = 0; $i < $dataCount; $i += $batchCount) {
            $dataItems = $modelName::getEsIndexDataItems($page, $batchCount);
            foreach ($dataItems as $itemModel) {
                $esEntity = new $esEntityName($itemModel);
                // list($preDividendTodayMoney, $preDividendTodayDate) = PreDividendTodayModel::getGoodsPreDividendTodayInfo($esEntity->getGId());
                // $esEntity->setPreTodayMoney($preDividendTodayMoney);
                // $esEntity->setPreTodayMoneyDate($preDividendTodayDate);
                list($resp, $errCode, $errMsg) = ElasticSearchService::indexWriteFromEsEntity($indexName, $esEntity);
                if ($errMsg != 'ok' || $errCode != 200) {
                    $this->wln("INDEX FAILED\nReason: \n". $errMsg);
                }
                if (isset($resp['success']) && $resp['success']) {
                    $successCount++;
                    // $esEntity->setIsSynchro($modelName::ES_SYNC_STATE_SYNCED);
                    $esEntity->save();
                }
            }
            $page++;
        }
        $this->wln('Finished');
        $this->wln("IndexedCount:\n". $successCount);
    }

    protected function indexDelete(string $indexName)
    {
        if (! $indexName) {
            return $this->wln("Index name Required");
        }
        $params = [
            'index_name' => $indexName,
        ];
        list($resp, $errCode, $errMsg) = ElasticSearchService::indexExists($params);
        if ($errMsg != 'ok' || $errCode != 200) {
            return $this->wln("INDEX EXISTS DETECT FAILED\nReason: \n". $errMsg);
        }
        if (isset($resp['exists']) && ! $resp['exists']) {
            return $this->wln('Index "'. $indexName. '" NOT EXSITS');
        }
        list($resp, $errCode, $errMsg) = ElasticSearchService::indexDelete($indexName, $params);
        if ($errMsg != 'ok' || $errCode != 200) {
            return $this->wln("DELETE INDEX '". $indexName. "' FAILED\nReason: \n". $errMsg);
        }
        if (isset($resp['success'])) {
            return $this->wln('Delete Index "'. $indexName. '" SUCCESS');
        }
        return $this->wln('DELETE INDEX FAILED');
    }

    protected function indexExists(string $indexName)
    {
        if (! $indexName) {
            return $this->wln("Index name Required");
        }
        $params = [
            'index_name' => $indexName,
        ];
        list($resp, $errCode, $errMsg) = ElasticSearchService::indexExists($params);
        if ($errMsg != 'ok' || $errCode != 200) {
            return $this->wln("INDEX EXISTS DETECT FAILED\nReason: \n". $errMsg);
        }
        if (isset($resp['exists']) && $resp['exists']) {
            return $this->wln('Index "'. $indexName. '" exists');
        }
        return $this->wln('INDEX "'. $indexName. '" NOT exists');
    }

    protected function indexRefresh(string $indexName)
    {
        if (! $indexName) {
            return $this->wln("Index name Required");
        }
        $params = [
            'index_name' => $indexName,
        ];
        list($resp, $errCode, $errMsg) = ElasticSearchService::indexExists($params);
        if ($errMsg != 'ok' || $errCode != 200) {
            return $this->wln("INDEX EXISTS DETECT FAILED\nReason: \n". $errMsg);
        }
        if (isset($resp['exists']) && ! $resp['exists']) {
            return $this->wln('Index "'. $indexName. '" NOT exists');
        }
        $modelName = EsConstants::MODEL_MAP[$indexName] ?? '';
        if (! $modelName) {
            return $this->wln('Refresh data model not found');
        }
        $esEntityName = EsConstants::ES_ENTITY_MAP[$indexName] ?? '';
        if (! $esEntityName) {
            return $this->wln('Refresh data entity not found');
        }
        $dataCount = $modelName::getEsRefreshableDataCount();
        if (! $dataCount) {
            return $this->wln("No data to refresh");
        }
        $this->wln('DataCount:');
        $this->wln($dataCount);
        $this->wln('Please wait');
        $batchCount = 100;
        $successCount = 0;
        $page = 1;
        for ($i = 0; $i < $dataCount; $i += $batchCount) {
            $dataItems = $modelName::getEsRefreshableDataItems($page, $batchCount);
            foreach ($dataItems as $itemModel) {
                $esEntity = new $esEntityName($itemModel);
                // list($preDividendTodayMoney, $preDividendTodayDate) = PreDividendTodayModel::getGoodsPreDividendTodayInfo($esEntity->getGId());
                // $esEntity->setPreTodayMoney($preDividendTodayMoney);
                // $esEntity->setPreTodayMoneyDate($preDividendTodayDate);
                list($resp, $errCode, $errMsg) = ElasticSearchService::indexRefreshFromEsEntity($indexName, $esEntity);
                if ($errMsg != 'ok' || $errCode != 200) {
                    $this->wln("REFRESH FAILED\nReason: \n". $errMsg);
                }
                if (isset($resp['success']) && $resp['success']) {
                    $successCount++;
                    // $esEntity->setIsSynchro($modelName::ES_SYNC_STATE_SYNCED);
                    $esEntity->save();
                }
            }
            $page++;
        }
        $this->wln('Finished');
        $this->wln("RefreshedCount:\n". $successCount);
    }

    private function wln($content)
    {
        if (! self::$logoutput) {
            self::$logoutput = new Output();
        }
        self::$logoutput->writeln(strval($content));
    }
}
