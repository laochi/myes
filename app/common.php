<?php

/**
 * api 返回数据函数
 */
function jsonResp(int $code, string $msg = 'success', $data = null)
{
    return json(['code' => $code, 'msg' => $msg, 'data' => $data]);
}