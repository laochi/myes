<?php
declare (strict_types = 1);

namespace app\middleware;

use app\services\UserAuthService;

class CheckUserAuth
{
    public function handle($request, \Closure $next)
    {
        // if ($error = UserAuthService::checkUserToken($request)) {
        //     return jsonResp($error['code'], $error['msg']);
        // }
        // if ($error = UserAuthService::verifySignV2($request)) {
        //     return jsonResp($error['code'], $error['msg']);
        // }
        // if ($error = UserAuthService::userStateNormal($request)) {
        //     return jsonResp($error['code'], $error['msg']);
        // }
        return $next($request);
    }
}
