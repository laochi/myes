<?php
namespace app\model;

class GoodsModel extends BaseModel implements ModelInterface
{
    protected $table = 'dy_goods';

    public const ES_SYNC_STATE_UNSYNCED = 1;//未同步到ES
    public const ES_SYNC_STATE_SYNCED = 2;//已同步到ES

    public static function getEsIndexData(int $page = 0, int $limit = 1, bool $refresh = false)
    {
        $datetime = date('Ymd', time() - 86400);
        $query = self::alias('g');
        $query->where('g.del', 0);
        $query->where('g.s_under', 0);
        if ($refresh) {
            // $query->where('is_synchro', 1);
            $query->order('audit_time');
        }
        $query->leftJoin('dy_goods_category gc_lv3', 'g.c_id = gc_lv3.c_id AND gc_lv3.level = 3 AND gc_lv3.del = 1');
        $query->leftJoin('dy_goods_category gc_lv2', 'gc_lv3.pid = gc_lv2.c_id AND gc_lv2.level = 2 AND gc_lv2.del = 1');
        $query->leftJoin('dy_goods_category gc_lv1', 'gc_lv2.pid = gc_lv1.c_id AND gc_lv1.level = 1 AND gc_lv1.del = 1');
        // $query->leftJoin('dy_pre_dividend_today pdt', 'g.g_id = pdt.g_id AND pdt.type IN (3, 4) AND pdt.createdate = '. $datetime);
        $fields = [
            'g.id' => 'id',
            'g.g_id' => 'g_id',
            'g.name' => 'name',
            'g.keyword' => 'keyword',
            'g.descword' => 'descword',
            'gc_lv1.c_id' => 'cateid_lv1',
            'gc_lv2.c_id' => 'cateid_lv2',
            'gc_lv3.c_id' => 'cateid_lv3',
            'gc_lv1.name' => 'catename_lv1',
            'gc_lv2.name' => 'catename_lv2',
            'gc_lv3.name' => 'catename_lv3',
            'g.s_id' => 's_id',
            'g.partner' => 'partner',
            'g.current_price' => 'current_price',
            'g.region_name' => 'region_name',
            'g.payment_sum' => 'payment_sum',
            'g.partner_max' => 'partner_max',
            'g.release' => 'release',
            'g.votes' => 'votes',
            'g.postage' => 'postage',
            'g.dividend_ratio' => 'dividend_ratio',
            'g.weight' => 'weight',
            'g.lable' => 'lable',
            'g.region_id' => 'region_id',
            'g.new_goods_delivery_time' => 'new_goods_delivery_time',
            'g.profit_week_scale' => 'profit_week_scale',
            'g.shelf_time' => 'shelf_time',
            'g.sales_actual' => 'sales_actual',
            'g.chip' => 'chip',
            'g.status' => 'status',
            'g.c_id' => 'c_id',
            // 'SUM(pdt.pre_today_money)' => 'pre_today_money',
        ];
        $query->field($fields);
        $data = $query->paginate([
            'list_rows' => $limit,
            'page' => $page,
        ]);
        if ($page) {
            return $data->items();
        }
        return $data->total();
    }

    public static function getEsIndexDataCount()
    {
        return self::getEsIndexData();
    }

    public static function getEsIndexDataItems(int $page, int $limit)
    {
        $limit = $limit ?: 100;
        return self::getEsIndexData($page, $limit);
    }

    public static function getGoodsStockHolderTime(int $goodsId)
    {
        $query = self::alias('g');
        $query->where('g_id', $goodsId);
        return $query->value('shareholdertime');
    }

    public static function getEsRefreshableData(int $page = 0, int $limit = 1)
    {
        return self::getEsIndexData($page, $limit, true);
    }

    public static function getEsRefreshableDataCount()
    {
        return self::getEsRefreshableData();
    }

    public static function getEsRefreshableDataItems(int $page, int $limit)
    {
        $limit = $limit ?: 100;
        return self::getEsRefreshableData($page, $limit);
    }
}