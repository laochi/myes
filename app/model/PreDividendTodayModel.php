<?php
namespace app\model;

class PreDividendTodayModel extends BaseModel implements ModelInterface
{
    protected $table = 'dy_pre_dividend_today';

    public static function getGoodsPreDividendTodayInfo(int $goodsId) : array
    {
        $datetime = date('Ymd', time() - 86400);
        $query = self::alias('p');
        $query->where('g_id', $goodsId);
        $query->where('type', 'in', [3, 4]);
        $query->where('createdate', $datetime);
        $query->field('sum(pre_today_money) as pre_today_money');
        $today_jin = $query->select();
        if(!empty($today_jin) && $today_jin[0]['pre_today_money'] != null  && $today_jin[0]['pre_today_money']!=''){
            $thisweek_about_bonus = $today_jin[0]['pre_today_money'];
        }else{
            $thisweek_about_bonus = 0.00;
        }
        return [(float) $thisweek_about_bonus, (int) $datetime];
    }
}