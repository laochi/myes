<?php
namespace app\model;

class PartnerModel extends BaseModel implements ModelInterface
{
    protected $table = 'dy_partner';

    public static function isUserGoodsAgent(int $goodsId, int $userId) : bool
    {
        $query = self::alias('p');
        $query->where('g_id', $goodsId);
        $query->where('u_id', $userId);
        $query->where('iseffective', 1);
        $id = $query->value('id');
        return $id ? true : false;
    }

    public static function getGoodsPartnerCount(int $goodsId) : int
    {
        $query = self::alias('p');
        $query->where('g_id', $goodsId);
        $query->where('status', 2);
        $query->field('g_id');
        return (int) $query->count();
    }
}