<?php
namespace app\model;

use think\Model;

abstract class BaseModel extends Model
{
    protected static function new()
    {
        return new static();
    }
}