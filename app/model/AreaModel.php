<?php
namespace app\model;

use think\facade\Db;

class AreaModel extends BaseModel implements ModelInterface
{
    protected $table = 'dy_area';

    public static function getNameLikeId(string $regionName) : int
    {
        $query = self::alias('a');
        $query->where('name', 'like', '%'. $regionName. '%');
        return (int) $query->value('id');
    }

    public static function getRegionChildrenIds(int $regionId) : array
    {
        $regionIds = [];
        $lv2Ids = [];
        $region = Db::name('dy_area')->where('id', $regionId)->field(['id', 'level', 'pid'])->find();
        if ($region) {
            if ($region['level'] == 3) {
                return [$regionId];
            }
            if ($region['level'] == 1) {
                $where = ['pid' => $regionId];
                $lv2Children= Db::name('dy_area')->where($where)->field(['id', 'level', 'pid'])->select();
                if ($lv2Children) {
                    $lv2Ids = array_column($lv2Children->toArray(), 'id');
                    $regionIds = array_merge($regionIds, $lv2Ids);
                }
            } else {
                $lv2Ids = [$regionId];
            }
            $lv3Children = Db::table('dy_area')->where('pid', 'in', $lv2Ids)->field('id')->select();
            if ($lv3Children) {
                $lv3Ids = array_column($lv3Children->toArray(), 'id');
                $regionIds = array_merge($regionIds, $lv3Ids);
            }
            $regionIds[] = $regionId;
            $regionIds = array_filter(array_unique($regionIds));
        }
        return $regionIds;
    }
}