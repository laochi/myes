<?php
namespace app\model;

class GoodsPicModel extends BaseModel implements ModelInterface
{
    protected $table = 'dy_goods_pic';

    public static function getGoodsPic(int $goodsId)
    {
        $query = self::alias('g');
        $query->where('g_id', $goodsId);
        return $query->value('logo');
    }
}