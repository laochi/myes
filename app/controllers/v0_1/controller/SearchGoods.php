<?php
namespace app\controllers\v0_1\controller;

use think\facade\Request;
use app\controllers\v0_1\BaseController;
use app\services\ElasticSearchService;
use app\outputs\v0_1\output\builders\goods\common\Builder as GoodsCommonOutputBuilder;
use app\outputs\v0_1\output\builders\goods\namesuggest\Builder as GoodsNameSuggestOutputBuilder;

class SearchGoods extends BaseController
{
    public function miss()
    {
        return $this->jsonResp(404, 'SearchMissing');
    }

    public function common()
    {
        $inputs = Request::post([
            'name', 'page', 'pagesize', 'c_id', 'region_name', 'city_id', 'state',
            'chip', 'sales_actual', 'current_price', 'shelf_time', 'profit_week_scale', 'new_goods_delivery_time', 'dividend_ratio', 'votes',
        ], 'post');
        list($result, $errCode, $errMsg) = ElasticSearchService::goodsCommonSearch($inputs);
        if ($result) {
            $result = GoodsCommonOutputBuilder::buildDetail([
                'list' => $result['list'],
                'total' => $result['total'],
                'pageno' => $result['pageno'],
                'pagesize' => $result['pagesize'],
            ]);
        }
        return $this->jsonResp($errCode, $errMsg, $result);
    }

    public function nameSuggest()
    {
        $inputs = Request::only(['search_text'], 'post');
        list($result, $errCode, $errMsg) = ElasticSearchService::goodsNameSuggest($inputs);
        if ($result) {
            $result = GoodsNameSuggestOutputBuilder::buildDetail([
                'list' => $result,
            ]);
        }
        return $this->jsonResp($errCode, $errMsg, $result);
    }
}
