<?php
namespace app\controllers\v0_1\controller;

use app\controllers\v0_1\controller\BaseController;

class Miss extends BaseController
{
    public function miss()
    {
        return $this->jsonResp(404, 'API Miss');
    }
}