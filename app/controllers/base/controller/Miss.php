<?php
namespace app\controllers\base\controller;

use app\controllers\base\BaseController;

class Miss extends BaseController
{
    public function miss()
    {
        return $this->jsonResp(404, 'API Miss');
    }
}