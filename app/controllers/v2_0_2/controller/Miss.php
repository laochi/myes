<?php
namespace app\controller;

use app\controllers\v2_0_2\controller\BaseController;

class Miss extends BaseController
{
    public function miss()
    {
        return $this->jsonResp(404, 'API Miss');
    }
}