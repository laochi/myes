<?php
namespace app\controllers\v2_0_2\controller;

use think\facade\Request;
use app\controllers\v2_0_2\BaseController;
use app\services\ElasticSearchService;
use app\outputs\v2_0_2\output\builders\goods\common\Builder as GoodsCommonOutputBuilder;
use app\outputs\v2_0_2\output\builders\goods\namesuggest\Builder as GoodsNameSuggestOutputBuilder;

class SearchGoods extends BaseController
{
    public function miss()
    {
        return $this->jsonResp(404, 'SearchMissing');
    }

    public function common()
    {
        $inputs = Request::post([
            'name', 'page', 'pagesize', 'c_id', 'region_name', 'city_id', 'state', '_explain', '_debug', '_mode',
            'chip', 'sales_actual', 'current_price', 'shelf_time', 'profit_week_scale', 'new_goods_delivery_time', 'dividend_ratio', 'votes', 'profit_week_scale', 'price_filtrate'
        ], 'post');
        list($result, $errCode, $errMsg) = ElasticSearchService::goodsCommonSearch($inputs);
        return $this->jsonResp($errCode, $errMsg, GoodsCommonOutputBuilder::buildDetail($result));
    }

    public function nameSuggest()
    {
        $inputs = Request::only(['search_text'], 'post');
        list($result, $errCode, $errMsg) = ElasticSearchService::goodsNameSuggest($inputs);
        return $this->jsonResp($errCode, $errMsg, GoodsNameSuggestOutputBuilder::buildDetail($result));
    }
}
