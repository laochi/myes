<?php
namespace app\entity;

use app\model\GoodsModel;

class GoodsEntity extends BaseEntity implements EntityInterface
{
    protected const MODEL_CLASS = GoodsModel::class;
    protected $model;

    function __construct(GoodsModel $model = null)
    {
        parent::__construct($model);
    }

    public function getId() : int
    {
        return (int) $this->model->id;
    }

    public function setId(int $id) : void
    {
        $this->model->id = $id;
    }

    public function getGId() : int
    {
        return (int) $this->model->g_id;
    }

    public function setGId(int $gId) : void
    {
        $this->model->g_id = $gId;
    }

    public function getCId() : int
    {
        return (int) $this->model->c_id;
    }

    public function setCId(int $cId) : void
    {
        $this->model->c_id = $cId;
    }

    public function getName() : string
    {
        return (string) $this->model->name;
    }

    public function setName(string $name) : void
    {
        $this->model->name = $name;
    }

    public function getKeyword() : string
    {
        return (string) $this->model->keyword;
    }

    public function setKeyword(string $keyword) : void
    {
        $this->model->keyword = $keyword;
    }

    public function getDescword() : string
    {
        return (string) $this->model->descword;
    }

    public function setDescword(string $descword) : void
    {
        $this->model->descword = $descword;
    }
}