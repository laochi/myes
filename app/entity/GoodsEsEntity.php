<?php
namespace app\entity;

use app\model\GoodsModel;

class GoodsEsEntity extends BaseEntity implements EsEntityInterface
{
    protected const MODEL_CLASS = GoodsModel::class;
    protected $model;

    function __construct(GoodsModel $model = null)
    {
        parent::__construct($model);
    }

    public function getId() : int
    {
        return (int) $this->model->id;
    }

    public function setId(int $id) : void
    {
        $this->model->id = $id;
    }

    public function getCId() : int
    {
        return (int) $this->model->c_id;
    }

    public function setCId(int $cId) : void
    {
        $this->model->c_id = $cId;
    }

    public function getGId() : int
    {
        return (int) $this->model->g_id;
    }

    public function setGId(int $gId) : void
    {
        $this->model->g_id = $gId;
    }

    public function getName() : string
    {
        return (string) $this->model->name;
    }

    public function setName(string $name) : void
    {
        $this->model->name = $name;
    }

    public function getKeyword() : string
    {
        return (string) $this->model->keyword;
    }

    public function setKeyword(string $keyword) : void
    {
        $this->model->keyword = $keyword;
    }

    public function getDescword() : string
    {
        return (string) $this->model->descword;
    }

    public function setDescword(string $descword) : void
    {
        $this->model->descword = $descword;
    }

    public function getCategoryIdLv1() : int
    {
        return (int) $this->model->cateid_lv1;
    }

    public function setCategoryIdLv1(int $cateId) : void
    {
        $this->model->cateid_lv1 = $cateId;
    }

    public function getCategoryIdLv2() : int
    {
        return (int) $this->model->cateid_lv2;
    }

    public function setCategoryIdLv2(int $cateId) : void
    {
        $this->model->cateid_lv2 = $cateId;
    }

    public function getCategoryIdLv3() : int
    {
        return (int) $this->model->cateid_lv3;
    }

    public function setCategoryIdLv3(int $cateId) : void
    {
        $this->model->cateid_lv3 = $cateId;
    }

    public function getCategoryNameLv1() : string
    {
        return (string) $this->model->catename_lv1;
    }

    public function setCategoryNameLv1(string $cateName) : void
    {
        $this->model->catename_lv1 = $cateName;
    }

    public function getCategoryNameLv2() : string
    {
        return (string) $this->model->catename_lv2;
    }

    public function setCategoryNameLv2(string $cateName) : void
    {
        $this->model->catename_lv2 = $cateName;
    }

    public function getCategoryNameLv3() : string
    {
        return (string) $this->model->catename_lv3;
    }

    public function setCategoryNameLv3(string $cateName) : void
    {
        $this->model->catename_lv3 = $cateName;
    }

    public function getSId() : int
    {
        return (int) $this->model->s_id;
    }

    public function setSId(int $sId) : void
    {
        $this->model->s_id = $sId;
    }

    public function getPartner() : int
    {
        return (int) $this->model->partner;
    }

    public function setPartner(int $partner) : void
    {
        $this->model->partner = $partner;
    }

    public function getCurrentPrice() : float
    {
        return (float) $this->model->current_price;
    }

    public function setCurrentPrice(float $currentPrice) : void
    {
        $this->model->current_price = $currentPrice;
    }

    public function getRegionId() : int
    {
        return (int) $this->model->region_id;
    }

    public function setRegionId(int $regionId) : void
    {
        $this->model->region_id = $regionId;
    }

    public function getRegionName() : string
    {
        return (string) $this->model->region_name;
    }

    public function setRegionName(string $regionName) : void
    {
        $this->model->region_name = $regionName;
    }

    public function getPaymentSum() : int
    {
        return (int) $this->model->payment_sum;
    }

    public function setPaymentSum(int $paymentSum) : void
    {
        $this->model->payment_sum = $paymentSum;
    }

    public function getPartnerMax() : int
    {
        return (int) $this->model->partner_max;
    }

    public function setPartnerMax(int $partnerMax) : void
    {
        $this->model->partner_max = $partnerMax;
    }

    public function getRelease() : int
    {
        return (int) $this->model->release;
    }

    public function setRelease(int $release) : void
    {
        $this->model->release = $release;
    }

    public function getVotes() : int
    {
        return (int) $this->model->votes;
    }

    public function setVotes(int $votes) : void
    {
        $this->model->votes = $votes;
    }

    public function getPostage() : string
    {
        return (string) $this->model->postage;
    }

    public function setPostage(string $postage) : void
    {
        $this->model->postage = $postage;
    }

    public function getDividendRatio() : float
    {
        return (float) $this->model->dividend_ratio;
    }

    public function setDividendRatio(float $dividendRatio) : void
    {
        $this->model->dividend_ratio = $dividendRatio;
    }

    public function getWeight() : float
    {
        return (float) $this->model->weight;
    }

    public function setWeight(float $weight) : void
    {
        $this->model->weight = $weight;
    }

    public function getLable() : string
    {
        return (string) $this->model->lable;
    }

    public function setLable(string $lable) : void
    {
        $this->model->lable = $lable;
    }

    public function getNewGoodsDeliveryTime() : int
    {
        return (int) $this->model->new_goods_delivery_time;
    }

    public function setNewGoodsDeliveryTime(int $newGoodsDeliveryTime) : void
    {
        $this->model->new_goods_delivery_time = $newGoodsDeliveryTime;
    }

    public function getProfitWeekScale() : string
    {
        return (string) $this->model->profit_week_scale;
    }

    public function setProfitWeekScale(string $profitWeekScale) : void
    {
        $this->model->profit_week_scale = $profitWeekScale;
    }

    public function getShelfTime() : int
    {
        return (int) $this->model->shelf_time;
    }

    public function setShelfTime(int $shelfTime) : void
    {
        $this->model->shelf_time = $shelfTime;
    }
    public function getSalesActual() : int
    {
        return (int) $this->model->sales_actual;
    }

    public function setSalesActual(int $salesActual) : void
    {
        $this->model->sales_actual = $salesActual;
    }
    public function getChip() : int
    {
        return (int) $this->model->chip;
    }

    public function setChip(int $chip) : void
    {
        $this->model->chip = $chip;
    }
    public function getStatus() : int
    {
        return (int) $this->model->status;
    }

    public function setStatus(int $status) : void
    {
        $this->model->status = $status;
    }

    public function getIsSynchro() : int
    {
        return (int) $this->model->is_synchro;
    }

    public function setIsSynchro(int $isSynchro) : void
    {
        $this->model->is_synchro = $isSynchro;
    }

    public function getPreTodayMoney() : float
    {
        return (float) $this->model->pre_today_money;
    }

    public function setPreTodayMoney(float $preTodayMoney) : void
    {
        $this->model->pre_today_money = $preTodayMoney;
    }

    public function getPreTodayMoneyDate() : int
    {
        return (int) $this->model->pre_today_date;
    }

    public function setPreTodayMoneyDate(int $preTodayDate) : void
    {
        $this->model->pre_today_date = $preTodayDate;
    }
}