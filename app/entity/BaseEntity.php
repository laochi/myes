<?php
namespace app\entity;

abstract class BaseEntity
{
    function __construct($model)
    {
        self::assignModel($model);
    }

    private static function getModelClass()
    {
        return static::MODEL_CLASS;
    }

    private static function createModelObject()
    {
        $modelClass = self::getModelClass();
        return new $modelClass;
    }

    private function assignModel($model = null)
    {
        $modelClass = $this->getModelClass();
        if ($model instanceof $modelClass) {
            $this->model = $model;
        } else {
            $this->model = new $modelClass;
        }
        return $this->model;
    }

    public function getModel()
    {
        return $this->model;
    }

    //通过主键获取
    public static function findByPk(int $id)
    {
        $modelObject = self::createModelObject();
        $pk = $modelObject->getPk();
        $model = $modelObject->where($pk, $id)->find();
        return new static($model);
    }

    //通过字段获取
    public static function findByColumn(string $column, $value)
    {
        $modelObject = self::createModelObject();
        $model = $modelObject->where($column, $value)->find();
        return new static($model);
    }

    public function save()
    {
        if (! $this->getModel()) {
            $this->createModelObject();
        }
        return $this->model->save();
    }
}