<?php
namespace app\services;

use app\Request;
use factory\Redis as RedisFactory;
use app\model\UserModel;

class UserAuthService
{
    public static function getUserId() : int
    {
        return (int) request()->param('user_id');
    }

    public static function checkUserToken(Request $request)
    {
        if (! $request->has('user_id') || ! $userId = $request->param('user_id')) {
            return self::buildError(201, '请重新登录(1001)');
        }
        if (! $request->has('user_token') || ! $userToken = $request->param('user_token')) {
            return self::buildError(201, '请重新登录(1002)');
        }

        $redis = RedisFactory::getInstance();
        if ($redis->get('user_token_:' . $userId) == $userToken) {
            return [];
        } else {
            return self::buildError(201, '请重新登录(1003)');
        }
    }

    public static function verifySignV2(Request $request)
    {
        if (! $request->has('newsign') || ! $sign = $request->param('newsign')) {
            return self::buildError(201, '签名校验失败(1004)');
        }

        if (! $request->has('user_id') || ! $userId = $request->param('user_id')) {
            return self::buildError(201, '签名校验失败(1005)');
        }

        if (! $request->has('dateline') || ! $dateline = $request->param('dateline')) {
            return self::buildError(201, '签名校验失败(1006)');
        }
        $userSecret = self::getUserSecret($userId);
        if (! $userSecret) {
            return self::buildError(201, '签名校验失败(1007)');
        }

        $data = $request->param();
        unset($data['newsign']);
        $data['dateline'] = intval($data['dateline']) + 1;
        ksort($data);
        $params = '';
        if(! empty($data)) {
            foreach ($data as $key => $val) {
                $params = $params. $key. '='. $val. '&';
            }
        }
        $params = substr($params, 0, -1);
        $sign2 = md5($params. $userSecret);
        if ($sign == $sign2) {
            return [];
        }
        return self::buildError(201, '签名校验失败(1008)'. $sign2);
    }

    public static function userStateNormal(Request $request)
    {
        if (! $request->has('user_id') || ! $userId = $request->param('user_id')) {
            return self::buildError(201, '请重新登录(1008)');
        }
        $state = UserModel::where('id', $userId)->value('state');
        if ($state == 1) {
            return [];
        }
        return self::buildError(201, '用户状态异常(1009)');
    }

    public static function getUserSecret(int $userId)
    {
        return UserModel::where('id', $userId)->value('secret');
    }

    private static function buildError(int $code, string $msg = '')
    {
        return [
            'code' => $code,
            'msg' => $msg,
        ];
    }
}