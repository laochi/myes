<?php
namespace app\services;

use app\services\ElasticSearch\Handler as EsHandler;
use app\entity\EsEntityInterface;

class ElasticSearchService
{
    public static function goodsCommonSearch(array $params = [])
    {
        $esHandler = new EsHandler();
        $esHandler->setIndexName('goods');
        $esHandler->setRequestParams($params);
        $esHandler->setRequestClassCode('common.search');
        $esHandler->setResponseClassCode('common.search');
        $esHandler->hideException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    public static function goodsNameSuggest(array $params = [])
    {
        $esHandler = new EsHandler();
        $esHandler->setIndexName('goods');
        $esHandler->setRequestParams($params);
        $esHandler->setRequestClassCode('name.suggest');
        $esHandler->setResponseClassCode('name.suggest');
        $esHandler->hideException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    //暂时废弃
    public static function goodsDocUpdate(array $params = [])
    {
        $esHandler = new EsHandler();
        $esHandler->setIndexName('goods');
        $esHandler->setRequestParams($params);
        $esHandler->setRequestClassCode('document.update');
        $esHandler->setResponseClassCode('document.update');
        $esHandler->showException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    public static function indexCreate(string $indexName, array $params = [])
    {
        $esHandler = new EsHandler();
        $esHandler->setIndexName($indexName);
        $esHandler->setRequestParams($params);
        $esHandler->setRequestClassCode('index.create');
        $esHandler->setResponseClassCode('index.create');
        $esHandler->showException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    public static function indexDelete(string $indexName, array $params = [])
    {
        $esHandler = new EsHandler();
        $esHandler->setIndexName($indexName);
        $esHandler->setRequestParams($params);
        $esHandler->setRequestClassCode('index.delete');
        $esHandler->setResponseClassCode('index.delete');
        $esHandler->showException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    public static function indexWrite(string $indexName, array $params = [])
    {
        $esHandler = new EsHandler();
        $esHandler->setIndexName($indexName);
        $esHandler->setRequestParams($params);
        $esHandler->setRequestClassCode('index.write');
        $esHandler->setResponseClassCode('index.write');
        $esHandler->showException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    public static function indexWriteFromEsEntity(string $indexName, EsEntityInterface $esEntity)
    {
        $esHandler = new EsHandler();
        $esHandler->setIndexName($indexName);
        $esHandler->setRequestEsEntity($esEntity);
        $esHandler->setRequestClassCode('index.write');
        $esHandler->setResponseClassCode('index.write');
        $esHandler->showException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    public static function indexRefreshFromEsEntity(string $indexName, EsEntityInterface $esEntity)
    {
        $esHandler = new EsHandler();
        $esHandler->setIndexName($indexName);
        $esHandler->setRequestEsEntity($esEntity);
        $esHandler->setRequestClassCode('index.refresh');
        $esHandler->setResponseClassCode('index.refresh');
        $esHandler->showException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    public static function serverInfo(array $params = [])
    {
        $esHandler = new EsHandler();
        $esHandler->setRequestParams($params);
        $esHandler->setRequestClassCode('server.info.show');
        $esHandler->setResponseClassCode('server.info.show');
        $esHandler->showException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }

    public static function indexExists(array $params = [])
    {
        $esHandler = new EsHandler();
        $esHandler->setRequestParams($params);
        $esHandler->setRequestClassCode('server.index.exists');
        $esHandler->setResponseClassCode('server.index.exists');
        $esHandler->showException();
        $esHandler->checkAndSendRequest();
        return $esHandler->makeResult();
    }
}