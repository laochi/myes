<?php
namespace app\services\ElasticSearch;

use think\facade\Env;
use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use Elasticsearch\Common\Exceptions\ServerErrorResponseException;

abstract class RequestAbstract
{
    private static $client;
    protected $tpl;
    protected $defaultPageNo = 1;
    protected $defaultSearchSize = 10;
    protected $debugMode = false;
    protected $debugData;

    protected function getTplArr()
    {
        return $this->tpl;
    }

    protected function useSearch(array $params) : array
    {
        $client = $this->getClient();
        $resp = $client->search($params);
        return $resp ?: [];
    }

    protected function useSuggest(array $params) : ?array
    {
        $client = $this->getClient();
        $resp = $client->search($params);
        return $resp ?: [];
    }

    protected function indexCreate(array $params) : ?array
    {
        $client = $this->getClient();
        $resp = $client->indices()->create($params);
        return $resp ?: [];
    }

    protected function indexDelete(array $params) : ?array
    {
        $client = $this->getClient();
        $resp = $client->indices()->delete($params);
        return $resp ?: [];
    }

    //暂未用到
    protected function update(array $params)
    {
        $client = $this->getClient();
        $resp = $client->update($params);
        return $resp ?: [];
    }

    protected function indexWrite(array $params)
    {
        $client = $this->getClient();
        $resp = $client->index($params);
        return $resp ?: [];
    }

    protected function serverInfo(array $params)
    {
        $client = $this->getClient();
        $resp = $client->info($params);
        return $resp ?: [];
    }

    protected function indexExists(array $params)
    {
        $client = $this->getClient();
        $resp = $client->indices()->exists($params);
        return $resp ? [
                'exists' => 1,
            ] : [
                'exists' => 0,
            ];
    }

    private function getClient()
    {
        if (self::$client) {
            return self::$client;
        }
        try {
            $confs = Constants::CONNECTIONS;
            $hosts = [
                [
                    'host' => Env::get($confs['host']),
                    'port' => Env::get($confs['port']),
                    'scheme' => Env::get($confs['scheme']),
                    // 'user' => Env::get($confs['user']),
                    // 'pass' => Env::get($confs['pass']),
                ]
            ];
            return self::$client = ClientBuilder::create()
                ->setHosts($hosts)
                ->setSSLVerification(false)
                ->build();
        } catch (BadRequest400Exception $e) {
            Helper::logException($e);
        } catch (ServerErrorResponseException $e) {
            Helper::logException($e);
        } catch (\Throwable $e) {
            Helper::logException($e);
        }
    }

    protected function calculatePageValues(array $params = [])
    {
        $pageNo = $params['page'] ?? $this->defaultPageNo;
        $pageSize = $params['pagesize'] ?? $this->defaultSearchSize;

        $offset = ($pageNo - 1) * $pageSize;
        return [$offset, $pageSize, $pageNo];
    }

    protected function getDebugData()
    {
        return $this->debugData;
    }

    protected function addDebugData(string $keyName, $data = null)
    {
        $this->debugData[$keyName] = $data;
    }
}