<?php
namespace app\services\ElasticSearch;

use app\services\ElasticSearch\Constants as EsConstants;
use app\services\ElasticSearch\Helper as EsHelper;
use app\entity\EsEntityInterface;

class Handler
{
    private $indexName = '';
    private $requestClassCode = '';
    private $responseClassCode = '';
    private $requestParams = [];
    private $requestEsEntity;
    private $requestObj;
    private $responseObj;
    private $responseHandled = [];
    private $errorCode = 200;
    private $errorMsg = 'ok';
    private $showException = true;

    public function setIndexName(string $indexName)
    {
        $this->indexName = $indexName;
    }

    public function setRequestClassCode(string $classCode)
    {
        $this->requestClassCode = $classCode;
    }

    public function setRequestParams(array $params)
    {
        $this->requestParams = $params;
    }

    public function setRequestEsEntity(EsEntityInterface $esEntity)
    {
        $this->requestEsEntity = $esEntity;
    }

    public function convertEsEntityToRequestParams()
    {
        if ($this->requestEsEntity) {
            $this->requestParams = $this->requestObj->makeParamsFromEsEntity($this->indexName, $this->requestEsEntity);
        }
    }

    public function setResponseClassCode(string $classCode)
    {
        $this->responseClassCode = $classCode;
    }

    public function init()
    {
        try {
            $requestClassName = EsHelper::getClass($this->indexName, $this->requestClassCode);
            $this->requestObj = new $requestClassName();
            $responseClassName = EsHelper::getClass($this->indexName, $this->responseClassCode);
            $this->responseObj = new $responseClassName();
            $this->convertEsEntityToRequestParams();
            if ($this->hasError()) {
                return false;
            }
            return true;
        } catch (\Throwable $e) {
            EsHelper::logException($e);
        }
        return false;
    }

    public function hasError() : bool
    {
        if (! $this->requestObj) {
            $this->errorCode = 101;
            $this->errorMsg = 'RequestClassNotFound';
            return true;
        }
        if (! $this->responseObj) {
            $this->errorCode = 102;
            $this->errorMsg = 'ResponseClassNotFound';
            return true;
        }
        if ($error = $this->requestObj->hasParamError($this->requestParams)) {
            $this->errorCode = 200;
            $this->errorMsg = $error;
            return true;
        }
        return false;
    }

    public function getErrorCode() : int
    {
        return $this->errorCode;
    }

    public function getErrorMsg() : string
    {
        return $this->errorMsg;
    }

    public function showException()
    {
        $this->showException = true;
    }

    public function hideException()
    {
        $this->showException = false;
    }

    public function checkAndSendRequest()
    {
        if (! $this->init()) {
            return;
        }
        try {
            $this->requestObj->assignTpl($this->requestParams);
            $responseResult = $this->requestObj->sendRequest();
            $this->responseHandled = $this->requestObj->handleResponse($responseResult);
        } catch (\Throwable $e) {
            $this->errorCode = 200;
            if (! $this->getErrorMsg()) {
                $this->errorMsg = $e->getMessage();
            }
            EsHelper::logException($e);
        }
    }

    public function makeResult()
    {
        if (! $this->showException) {
            $this->errorMsg = '';
        }
        return [
            $this->responseHandled,
            $this->errorCode,
            $this->errorMsg,
        ];
    }
}