<?php
namespace app\services\ElasticSearch;

class Constants
{
    const CONNECTIONS = [
        'host' => 'ES_SERVER.HOST',
        'port' => 'ES_SERVER.PORT',
        'scheme' => 'ES_SERVER.SCHEME',
        'user' => 'ES_SERVER.USERNAME',
        'pass' => 'ES_SERVER.PASSWORD',
    ];

    const REQUESTS = [
        'goods.name.suggest' => 'app\services\ElasticSearch\Requests\goods\NameSuggest',
        'goods.common.search' => 'app\services\ElasticSearch\Requests\goods\CommonSearch',
        'goods.index.create' => 'app\services\ElasticSearch\Requests\goods\IndexCreate',
        'goods.index.delete' => 'app\services\ElasticSearch\Requests\goods\IndexDelete',
        'goods.index.write' => 'app\services\ElasticSearch\Requests\goods\IndexWrite',
        'goods.index.refresh' => 'app\services\ElasticSearch\Requests\goods\IndexRefresh',
        'server.info.show' => 'app\services\ElasticSearch\Requests\server\InfoShow',
        'server.index.exists' => 'app\services\ElasticSearch\Requests\server\IndexExists',
    ];

    const MODEL_MAP = [
        'goods' => 'app\model\GoodsModel',
    ];

    const ENTITY_MAP = [
        'goods' => 'app\entity\GoodsEntity',
    ];

    const ES_ENTITY_MAP = [
        'goods' => 'app\entity\GoodsEsEntity',
    ];

    const INDEX_CREATE_JSON_MAP = [
        'goods' => self::INDEX_CREATION_JSON_GOODS,
    ];

    const INDEX_CREATION_JSON_GOODS = <<<JSON
    {
        "settings": {
            "analysis": {
                "analyzer": {
                    "prefix_pinyin_analyzer": {
                        "tokenizer": "standard",
                        "filter": [
                            "lowercase",
                            "prefix_pinyin"
                        ]
                    },
                    "full_pinyin_analyzer": {
                        "tokenizer": "standard",
                        "filter": [
                            "lowercase",
                            "full_pinyin"
                        ]
                    }
                },
                "filter":{
                    "prefix_pinyin": {
                        "type": "pinyin",
                        "keep_first_letter": true,
                        "keep_full_pinyin": false,
                        "none_chinese_pinyin_tokenize": false,
                        "keep_original": false
                    },
                    "full_pinyin": {
                        "type": "pinyin",
                        "keep_first_letter": false,
                        "keep_full_pinyin": true,
                        "keep_original": false,
                        "keep_none_chinese_in_first_letter": false
                    }
                }
            }
        },
        "mappings": {
            "properties": {
    
                "g_id":{
                  "type": "integer"
                },

                "name": {
                    "type": "text",
                    "fields": {
                        "suggest_text": {
                            "type": "completion",
                            "analyzer": "standard",
                            "preserve_separators": false,
                            "preserve_position_increments": true,
                            "max_input_length": 50
                        },
                        "prefix_pinyin": {
                            "type": "completion",
                            "analyzer": "prefix_pinyin_analyzer",
                            "search_analyzer": "standard",
                            "preserve_separators": false
                        },
                        "full_pinyin": {
                            "type": "completion",
                            "analyzer": "full_pinyin_analyzer",
                            "search_analyzer": "full_pinyin_analyzer",
                            "preserve_separators": false
                        }
                    }
                },

                "keyword": {
                    "type": "text",
                },

                "descword": {
                    "type": "text",
                },

                "cateidlv1":{
                  "type": "integer"
                },

                "cateidlv2":{
                  "type": "integer"
                },

                "cateidlv3":{
                  "type": "integer"
                },

                "catenamelv2": {
                    "type": "text",
                },

                "catenamelv3": {
                    "type": "text",
                },

                "s_id": {
                    "type": "integer"
                },
                "region_id": {
                    "type": "integer"
                },
                "partner": {
                    "type": "integer"
                },
                "current_price": {
                    "type": "double"
                },
                "region_name": {
                    "type": "text"
                },
                "payment_sum": {
                    "type": "integer"
                },
                "partner_max": {
                    "type": "integer"
                },
                "release": {
                    "type": "integer"
                },
                "votes": {
                    "type": "integer"
                },
                "postage": {
                    "type": "text"
                },
                "dividend_ratio": {
                    "type": "double"
                },
                "weight": {
                    "type": "double"
                },
                "lable": {
                    "type": "text"
                },
                "status": {
                    "type": "integer"
                },
                "chip": {
                    "type": "integer"
                },
                "sales_actual": {
                    "type": "integer"
                },
                "shelf_time": {
                    "type": "integer"
                },
                "profit_week_scale": {
                    "type": "double"
                },
                "new_goods_delivery_time": {
                    "type": "integer"
                },
                "pre_today_money": {
                    "type": "double"
                },
                "pre_today_money_date": {
                    "type": "integer"
                },
                "c_id": {
                    "type": "integer"
                }
             }
        }
    }
JSON;
}