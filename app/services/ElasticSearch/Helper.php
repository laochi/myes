<?php
namespace app\services\ElasticSearch;

use think\facade\Log;
use app\services\ElasticSearch\Constants as EsConstants;
use app\services\ElasticSearch\Requests;

class Helper
{
    public static function logException($e) : void
    {
        Log::write(sprintf("\n[Exception]\nFile:\n%s\nLine:\n%s\nMessage:\n%s\n", $e->getFile(), $e->getLine(), $e->getMessage()));
    }

    public static function isPinyinPrefix(string $string) : bool
    {
        $string = trim($string);
        $pattern = '/^[a-z]+/i';
        if (preg_match($pattern, $string)) {
            return true;
        }
        return false;
    }

    public static function hasPinyin(string $string)
    {
        $string = trim($string);
        $pattern = '/[a-z]+/i';
        if (preg_match($pattern, $string)) {
            return true;
        }
        return false;
    }

    public static function hasChinese(string $string)
    {
        $string = trim($string);
        if (preg_match('/[\x7f-\xff]/', $string)) {
            return true;
        }
        return false;
    }

    public static function getClass(string $indexName, string $classCode) : string
    {
        if ($indexName) {
            $classCode = $indexName. '.'. $classCode;
        }
        return EsConstants::REQUESTS[$classCode] ?? '';
    }
}