<?php
namespace app\services\ElasticSearch;

interface RequestInterface
{
    public function assignTpl(array $params = []);
    
    public function handleResponse(array $response = []);
    
    public function sendRequest();
    
    public function hasParamError(array $params = []);
}