<?php
namespace app\services\ElasticSearch\Requests\goods;

use app\services\ElasticSearch\RequestAbstract;
use app\services\ElasticSearch\RequestInterface;
use app\services\ElasticSearch\Helper as EsHelper;
use app\entity\GoodsEsEntity;
use app\model\AreaModel;

class CommonSearch extends RequestAbstract implements RequestInterface
{
    private $pageno = 1;
    private $pagesize = 10;
    // private $sortField = '';
    private $explainMode = false;
    private $wordMode = 1;

    public $tpl = [
        'index' => 'goods',
        'type' => '_doc',
        'body' => [
            'explain' => false,
            'query' => [
                'bool' => [
                    'filter' => [],
                    'must' => [],
                ],
            ],
            'from' => 0,
            'size' => 10,
            'sort' => [],
        ],
    ];

    public function assignTpl(array $params = [])
    {
        if ($wordMode = $params['_mode'] ?? null) {
            $this->wordMode = $wordMode;
        }
        if ($params['_explain'] ?? null) {
            $this->explainMode = true;
            $this->tpl['body']['explain'] = true;
        }
        if ($params['_debug'] ?? null) {
            $this->debugMode = true;
            $this->addDebugData('params', $params);
        }
        $searchText = $params['name'] ?? '';
        $searchText = trim($searchText);

        $priceFilter = $params['price_filtrate'] ?? '';

        if ($searchText) {
            $searchText = urldecode($searchText);
            foreach (explode(' ', $searchText) as $searchTextItem) {
                if (! trim($searchTextItem)) {
                    continue;
                }
                $hasPinyin = EsHelper::hasPinyin($searchTextItem);
                $hasChinese = EsHelper::hasChinese($searchTextItem);

                //同时含有拼音和汉字
                if ($hasPinyin && $hasChinese) {
                    $mustBoolShouldWholeWord = [
                        ['match_phrase' => ['name' => ['query' =>$searchTextItem, 'boost' => 10]]],
                        ['match_phrase' => ['keyword' => ['query' =>$searchTextItem, 'boost' => 10]]],
                        ['match_phrase' => ['catenamelv2' => ['query' =>$searchTextItem, 'boost' => 1]]],
                        ['match_phrase' => ['catenamelv3' => ['query' =>$searchTextItem, 'boost' => 5]]],
                        ['match_phrase' => ['name.pinyin' => $searchTextItem]],
                        ['match_phrase' => ['keyword.pinyin' => $searchTextItem]],
                        ['match_phrase' => ['catenamelv2.pinyin' => $searchTextItem]],
                        ['match_phrase' => ['catenamelv3.pinyin' => $searchTextItem]],
                    ];
                    $mustBoolShouldSplitWord = [
                        ['match' => ['name' => ['query' =>$searchTextItem, 'boost' => 1]]],
                        ['match' => ['keyword' => ['query' =>$searchTextItem, 'boost' => 1]]],
                        ['match' => ['catenamelv3' => ['query' =>$searchTextItem, 'boost' => 1]]],
                    ];

                //只有拼音
                } else if ($hasPinyin) {
                    $mustBoolShouldWholeWord = [
                        ['match_phrase' => ['name.pinyin' => ['query' => $searchTextItem, 'boost' => 5]]],
                        ['match_phrase' => ['keyword.pinyin' => ['query' => $searchTextItem, 'boost' => 5]]],
                        ['match_phrase' => ['catenamelv2.pinyin' => $searchTextItem]],
                        ['match_phrase' => ['catenamelv3.pinyin' => ['query' => $searchTextItem, 'boost' => 5]]],
                    ];
                    $mustBoolShouldSplitWord = [
                        ['match' => ['keyword.pinyin' => ['query' =>$searchTextItem]]],
                    ];

                //只有汉字
                } else {
                    $mustBoolShouldWholeWord = [
                        ['match_phrase' => ['name' => ['query' => $searchTextItem, 'boost' => 5]]],
                        ['match_phrase' => ['keyword' => ['query' =>$searchTextItem, 'boost' => 10]]],
                        ['match_phrase' => ['catenamelv2' => ['query' => $searchTextItem, 'boost' => 1]]],
                        ['match_phrase' => ['catenamelv3' => ['query' => $searchTextItem, 'boost' => 5]]],
                    ];
                    $mustBoolShouldSplitWord = [
                        ['match' => ['keyword' => ['query' =>$searchTextItem]]],
                    ];
                }

                //整词匹配
                if ($this->wordMode == 1) {
                    $mustItem = [
                        'bool' => [
                            'should' => $mustBoolShouldWholeWord,
                        ],
                    ];

                //整词匹配分词补充
                } else {
                    $mustItem = [
                        'bool' => [
                            'should' => [
                                ['bool' => [
                                    'should' => $mustBoolShouldWholeWord,
                                ]],
                                ['bool' => [
                                    'should' => $mustBoolShouldSplitWord,
                                ]],
                            ],
                        ],
                    ];
                }

                $this->tpl['body']['query']['bool']['must'][] = $mustItem;
            }//foreach
        } else {
            unset($this->tpl['body']['query']['bool']['must']);
        }

        $state = $params['state'] ?? '';
        if ($state && $state == 2) {
            $status = 3;
        } else {
            $status = 1;
        }

        $this->tpl['body']['query']['bool']['filter'][] = [
            'term' => [
                'status' => $status,
            ],
        ];

        if ($cId = $params['c_id'] ?? 0) {
            $this->tpl['body']['query']['bool']['filter'][] = [
                'term' => [
                    'c_id' => $cId
                ],
            ];
        }

        if ($priceFilter && is_int(strpos($priceFilter, '-'))) {
            list($minPrice, $maxPrice) = explode('-', $priceFilter);
            $this->tpl['body']['query']['bool']['filter'][] = [
                'range' => [
                    'current_price' => [
                        'gte' => intval($minPrice),
                        'lte' => intval($maxPrice),
                    ],
                ],
            ];
        }

        list($from, $size, $page) = $this->calculatePageValues($params);
        $this->tpl['body']['from'] = $from;
        $this->tpl['body']['size'] = $size;
        $this->pagesize = $size;
        $this->pageno = $page;

        $regionName = $params['region_name'] ?? '';
        $regionName = trim($regionName);
        $regionName = urldecode($regionName);
        if ($regionName) {
            if ($regionName == '全国') {
                $cityId = 0;
            } else {
                if (! preg_match("/^[\x{4e00}-\x{9fa5}]+$/u", $regionName)) {
                    $this->tpl = [];
                    return;
                }
                $cityId = AreaModel::getNameLikeId($regionName);
            }
        } else if ($cityId = $params['city_id'] ?? '0') {
            $cityId = intval($cityId);
        }

        if ($cityId) {
            $regionIds = AreaModel::getRegionChildrenIds($cityId);
            if ($regionIds) {
                $regionArr = [];
                foreach ($regionIds as $regionId) {
                    $regionArr[] = [
                        'term' => ['region_id' => $regionId],
                    ];
                }
                $this->tpl['body']['query']['bool']['filter'][] = [
                    'bool' => [
                        'should' => $regionArr,
                    ],
                ];
            }
        }

        $sort = [];
        if ($chip = $params['chip'] ?? null) {//筹码
            $sort[] = ['chip' => ['order' => $chip]];
        } else if ($salesActual = $params['sales_actual'] ?? null) {//销量
            $sort[] = ['sales_actual' => ['order' => $salesActual]];
        } else if ($currentPrice = $params['current_price'] ?? null) {//价格
            $sort[] = ['current_price' => ['order' => $currentPrice]];
        } else if ($shelfTime = $params['shelf_time'] ?? null) {//最新上架
            $sort[] = ['shelf_time' => ['order' => $shelfTime]];
        } else if ($thisweekAboutBonus = $params['profit_week_scale'] ?? null) {//今日分红
            // $sort[] = ['profit_week_scale' => ['order' => $profitWeekScale]];
             $sort[] = ['pre_today_money' => ['order' => $thisweekAboutBonus]];
        } else if ($newGoodsDeliveryTime = $params['new_goods_delivery_time'] ?? null) {//
            $sort[] = ['new_goods_delivery_time' => ['order' => $newGoodsDeliveryTime]];
        } else if ($dividendRatio = $params['dividend_ratio'] ?? null) {
            $sort[] = ['dividend_ratio' => ['order' => $dividendRatio]];
        } else if ($votes = $params['votes'] ?? null) {//票数
            $sort[] = ['votes' => ['order' => $votes]];
        // } else if ($thisweekAboutBonus = $params['thisweek_about_bonus'] ?? null) {
        //     $sort[] = ['pre_today_money' => ['order' => $thisweekAboutBonus]];
        }
        $sort[] = ['_score' => ['order' => 'desc']];
        $this->tpl['body']['sort'] = $sort;
    }

    public function handleResponse(array $response = [])
    {
        if ($this->explainMode) {
            print_r($response);exit;
        }
        $responseData = [
            'pageno' => $this->pageno,
            'pagesize' => $this->pagesize,
            // 'sort' => $this->sortField,
            'total' => 0,
            'list' => [],
        ];
        $responseData['pageno'] = $this->pageno;
        $responseData['pagesize'] = $this->pagesize;
        if (isset($response['hits']) && $hits = $response['hits']) {
            $totalHits = 0;
            if (isset($hits['total']) && isset($hits['total']['value'])) {
                $totalHits = intval($hits['total']['value']);
                $responseData['total'] = $totalHits;
            }
            if (isset($hits['hits']) && $hitsArr = $hits['hits']) {
                foreach ($hitsArr as $hitItem) {
                    $goodsEsEntity = new GoodsEsEntity;
                    $responseData['total'] = $totalHits;
                    if (isset($hitItem['_source']) && $source = $hitItem['_source']) {
                        $goodsEsEntity->setGId($source['g_id']);
                        $goodsEsEntity->setName($source['name']);
                        $goodsEsEntity->setKeyword($source['keyword']);
                        $goodsEsEntity->setDescword($source['descword']);
                        $goodsEsEntity->setCategoryIdLv1(intval($source['cateidlv1']));
                        $goodsEsEntity->setCategoryIdLv2(intval($source['cateidlv2']));
                        $goodsEsEntity->setCategoryIdLv3(intval($source['cateidlv3']));
                        $goodsEsEntity->setCategoryNameLv1($source['catenamelv1']);
                        $goodsEsEntity->setCategoryNameLv2($source['catenamelv2']);
                        $goodsEsEntity->setCategoryNameLv3($source['catenamelv3']);
                        $goodsEsEntity->setSId($source['s_id']);
                        $goodsEsEntity->setPartner($source['partner']);
                        $goodsEsEntity->setCurrentPrice($source['current_price']);
                        $goodsEsEntity->setRegionName($source['region_name']);
                        $goodsEsEntity->setPaymentSum($source['payment_sum']);
                        $goodsEsEntity->setPartnerMax($source['partner_max']);
                        $goodsEsEntity->setRelease($source['release']);
                        $goodsEsEntity->setVotes($source['votes']);
                        $goodsEsEntity->setPostage($source['postage']);
                        $goodsEsEntity->setDividendRatio($source['dividend_ratio']);
                        $goodsEsEntity->setWeight($source['weight']);
                        $goodsEsEntity->setLable($source['lable']);
                        $goodsEsEntity->setRegionId(intval($source['region_id']));
                        $goodsEsEntity->setNewGoodsDeliveryTime(intval($source['new_goods_delivery_time']));
                        $goodsEsEntity->setProfitWeekScale($source['profit_week_scale']);
                        $goodsEsEntity->setShelfTime(intval($source['shelf_time']));
                        $goodsEsEntity->setSalesActual(intval($source['sales_actual']));
                        $goodsEsEntity->setChip(intval($source['chip']));
                        $goodsEsEntity->setStatus(intval($source['status']));
                        $goodsEsEntity->setCId(intval($source['c_id']));
                        $goodsEsEntity->setPreTodayMoney(floatval($source['pre_today_money']));
                        $goodsEsEntity->setPreTodayMoneyDate(floatval($source['pre_today_money_date']));
                    }
                    $responseData['list'][] = $goodsEsEntity;
                }//end foreach
            }
        }
        if ($this->debugMode) {
            $responseData['debug'] = $this->getDebugData();
        }
        return $responseData;
    }

    public function sendRequest()
    {
        return $this->useSearch($this->getTplArr());
    }

    public function hasParamError(array $params = [])
    {
        // if (! isset($params['name']) || '' == trim($params['name'])) {
        //     $this->errorMsg = 'EmptySearchText';
        //     return true;
        // }
        return false;
    }
}