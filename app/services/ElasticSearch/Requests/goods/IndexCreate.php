<?php
namespace app\services\ElasticSearch\Requests\goods;

use app\services\ElasticSearch\RequestAbstract;
use app\services\ElasticSearch\RequestInterface;

class IndexCreate extends RequestAbstract implements RequestInterface
{
    public $tpl = [
        'index' => '',
        'body' => [],
    ];

    public function assignTpl(array $params = [])
    {
        $indexName = $params['index_name'] ?? '';
        $json = $params['create_json'] ?? '';
        $this->tpl['index'] = $indexName;
        $this->tpl['body'] = json_decode($json, true);
    }

    public function handleResponse(array $response = [])
    {
        $responseData = [
            'success' => false,
            'index' => '',
        ];
        if (isset($response['acknowledged']) && $response['acknowledged']) {
            $responseData['success'] = true;
        }
        if (isset($response['index']) && $response['index']) {
            $responseData['index'] = $response['index'];
        }
        return $responseData;
    }

    public function sendRequest()
    {
        return $this->indexCreate($this->getTplArr());
    }

    public function hasParamError(array $params = [])
    {
        if (! isset($params['index_name']) || '' == trim($params['index_name'])) {
            $this->errorMsg = 'EmptyIndexName';
            return true;
        }
        if (! isset($params['create_json']) || '' == trim($params['create_json'])) {
            $this->errorMsg = 'EmptyCreateJson';
            return true;
        }
        return false;
    }
}