<?php
namespace app\services\ElasticSearch\Requests\goods;

use app\services\ElasticSearch\RequestAbstract;
use app\services\ElasticSearch\RequestInterface;
use app\entity\GoodsEsEntity;

class NameSuggest extends RequestAbstract implements RequestInterface
{
    public $tpl = [
        'index' => 'goods',
        'type' => '_doc',
        'body' => [
            'suggest' => [
                'text' => '',
                'prefix_pinyin' => [
                    'completion' => [
                        'field' => 'name.prefix_pinyin',
                        'size' => 10,
                        'skip_duplicates' => true,
                    ],
                ],
                'full_pinyin' => [
                    'completion' => [
                        'field' => 'name.full_pinyin',
                        'size' => 10,
                        'skip_duplicates' => true,
                    ],
                ],
            ],
        ],
    ];

    public function assignTpl(array $params = [])
    {
        $suggestText = $params['search_text'] ?? '';
        $suggestText = trim($suggestText);
        $suggestText = urldecode($suggestText);
        $this->tpl['body']['suggest']['text'] = $suggestText;

        list(
            $tmp,
            $this->tpl['body']['suggest']['prefix_pinyin']['completion']['size']
        ) = $this->calculatePageValues($params);
        list(
            $tmp,
            $this->tpl['body']['suggest']['full_pinyin']['completion']['size']
        ) = $this->calculatePageValues($params);

    }

    public function handleResponse(array $response = [])
    {
        $responseData = [];
        if (isset($response['suggest']) && $suggests = $response['suggest']) {
            foreach ($suggests as $sugFields) {
                if ($sugFields) {
                    foreach ($sugFields as $sugField) {
                        if (isset($sugField['options']) && $opts = $sugField['options']) {
                            foreach ($opts as $opt) {
                                if (isset($opt['_source']) && $source = $opt['_source']) {
                                    $goodsEsEntity = new GoodsEsEntity();
                                    $goodsEsEntity->setGId($source['g_id']);
                                    $goodsEsEntity->setName($source['name']);
                                    $goodsEsEntity->setKeyword($source['keyword']);
                                    $goodsEsEntity->setDescword($source['descword']);
                                    $goodsEsEntity->setCategoryIdLv1(intval($source['cateidlv1']));
                                    $goodsEsEntity->setCategoryIdLv2(intval($source['cateidlv2']));
                                    $goodsEsEntity->setCategoryIdLv3(intval($source['cateidlv3']));
                                    $goodsEsEntity->setCategoryNameLv1($source['catenamelv1']);
                                    $goodsEsEntity->setCategoryNameLv2($source['catenamelv2']);
                                    $goodsEsEntity->setCategoryNameLv3($source['catenamelv3']);
                                    $responseData[] = $goodsEsEntity;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $responseData;
    }

    public function sendRequest()
    {
        return $this->useSuggest($this->getTplArr());
    }

    public function hasParamError(array $params = [])
    {
        if (! isset($params['search_text']) || '' == trim($params['search_text'])) {
            $this->errorMsg = 'EmptySearchText';
            return true;
        }
        return false;
    }
}