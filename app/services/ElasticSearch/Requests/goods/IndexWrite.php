<?php
namespace app\services\ElasticSearch\Requests\goods;

use app\services\ElasticSearch\RequestAbstract;
use app\services\ElasticSearch\RequestInterface;
use app\entity\EsEntityInterface;

class IndexWrite extends RequestAbstract implements RequestInterface
{
    public $tpl = [
        'index' => '',
        'type' => '_doc',
        'id' => '',
        'body' => [
            'g_id' => '',
            'name' => '',
            'keyword' => '',
            'descword' => '',
            'cateidlv1' => '',
            'cateidlv2' => '',
            'cateidlv3' => '',
            'catenamelv1' => '',
            'catenamelv2' => '',
            'catenamelv3' => '',
            's_id' => '',
            'partner' => '',
            'current_price' => '',
            'region_name' => '',
            'payment_sum' => '',
            'partner_max' => '',
            'release' => '',
            'votes' => '',
            'postage' => '',
            'dividend_ratio' => '',
            'weight' => '',
            'lable' => '',
            'region_id' => '',
            'new_goods_delivery_time' => '',
            'profit_week_scale' => '',
            'shelf_time' => '',
            'sales_actual' => '',
            'chip' => '',
            'status' => '',
            'c_id' => '',
            'pre_today_money' => '',
            'pre_today_money_date' => '',
        ],
    ];

    public function assignTpl(array $params = [])
    {
        $indexName = $params['index_name'] ?? '';
        $this->tpl['index'] = $indexName;

        $gId = $params['g_id'] ?? '';
        $this->tpl['id'] = $gId;

        $this->tpl['body']['g_id'] = $gId;

        $name = $params['name'] ?? '';
        $this->tpl['body']['name'] = $name;
        $keyword = $params['keyword'] ?? '';
        $this->tpl['body']['keyword'] = $keyword;
        $descword = $params['descword'] ?? '';
        $this->tpl['body']['descword'] = $descword;

        $cateidLv1 = $params['cateidlv1'] ?? '';
        $this->tpl['body']['cateidlv1'] = $cateidLv1;
        $cateidLv2 = $params['cateidlv2'] ?? '';
        $this->tpl['body']['cateidlv2'] = $cateidLv2;
        $cateidLv3 = $params['cateidlv3'] ?? '';
        $this->tpl['body']['cateidlv3'] = $cateidLv3;

        $catenameLv1 = $params['catenamelv1'] ?? '';
        $this->tpl['body']['catenamelv1'] = $catenameLv1;
        $catenameLv2 = $params['catenamelv2'] ?? '';
        $this->tpl['body']['catenamelv2'] = $catenameLv2;
        $catenameLv3 = $params['catenamelv3'] ?? '';
        $this->tpl['body']['catenamelv3'] = $catenameLv3;

        $this->tpl['body']['s_id'] = $params['s_id'] ?? '';
        $this->tpl['body']['partner'] = $params['partner'] ?? '';
        $this->tpl['body']['current_price'] = $params['current_price'] ?? '';
        $this->tpl['body']['region_name'] = $params['region_name'] ?? '';
        $this->tpl['body']['payment_sum'] = $params['payment_sum'] ?? '';
        $this->tpl['body']['partner_max'] = $params['partner_max'] ?? '';
        $this->tpl['body']['release'] = $params['release'] ?? '';
        $this->tpl['body']['votes'] = $params['votes'] ?? '';
        $this->tpl['body']['postage'] = $params['postage'] ?? '';
        $this->tpl['body']['dividend_ratio'] = $params['dividend_ratio'] ?? '';
        $this->tpl['body']['weight'] = $params['weight'] ?? '';
        $this->tpl['body']['lable'] = $params['lable'] ?? '';

        $this->tpl['body']['region_id'] = $params['region_id'] ?? '';
        $this->tpl['body']['new_goods_delivery_time'] = $params['new_goods_delivery_time'] ?? '';
        $this->tpl['body']['profit_week_scale'] = floatval($params['profit_week_scale']) ?? 0.0;
        $this->tpl['body']['shelf_time'] = $params['shelf_time'] ?? '';
        $this->tpl['body']['sales_actual'] = $params['sales_actual'] ?? '';
        $this->tpl['body']['chip'] = $params['chip'] ?? '';
        $this->tpl['body']['status'] = $params['status'] ?? '';
        $this->tpl['body']['c_id'] = $params['c_id'] ?? '';
        $this->tpl['body']['pre_today_money'] = $params['pre_today_money'] ?? 0.0;
        $this->tpl['body']['pre_today_money_date'] = $params['pre_today_money_date'] ?? 0;
    }

    public function handleResponse(array $response = [])
    {
        $responseData = [
            'success' => false,
        ];
        if (isset($response['result']) && in_array($response['result'], ['created', 'updated'])) {
            $responseData['success'] = true;
        }
        return $responseData;
    }

    public function sendRequest()
    {
        return $this->indexWrite($this->getTplArr());
    }

    public function hasParamError(array $params = [])
    {
        if (! isset($params['index_name']) || '' == trim($params['index_name'])) {
            $this->errorMsg = 'EmptyIndexName';
            return true;
        }
        return false;
    }

    public function makeParamsFromEsEntity(string $indexName, EsEntityInterface $esEntity)
    {
        $params = [
            'index_name' => $indexName,
            'g_id' => $esEntity->getGId(),
            'name' => $esEntity->getName(),
            'keyword' => $esEntity->getKeyword(),
            'descword' => $esEntity->getDescword(),
            'catenamelv1' => $esEntity->getCategoryNameLv1(),
            'catenamelv2' => $esEntity->getCategoryNameLv2(),
            'catenamelv3' => $esEntity->getCategoryNameLv3(),
            'cateidlv1' => $esEntity->getCategoryIdLv1(),
            'cateidlv2' => $esEntity->getCategoryIdLv2(),
            'cateidlv3' => $esEntity->getCategoryIdLv3(),
            's_id' => $esEntity->getSId(),
            'partner' => $esEntity->getPartner(),
            'current_price' => $esEntity->getCurrentPrice(),
            'region_name' => $esEntity->getRegionName(),
            'payment_sum' => $esEntity->getPaymentSum(),
            'partner_max' => $esEntity->getPartnerMax(),
            'release' => $esEntity->getRelease(),
            'votes' => $esEntity->getVotes(),
            'postage' => $esEntity->getPostage(),
            'dividend_ratio' => $esEntity->getDividendRatio(),
            'weight' => $esEntity->getWeight(),
            'lable' => $esEntity->getLable(),
            'region_id' => $esEntity->getRegionId(),
            'new_goods_delivery_time' => $esEntity->getNewGoodsDeliveryTime(),
            'profit_week_scale' => $esEntity->getProfitWeekScale(),
            'shelf_time' => $esEntity->getShelfTime(),
            'sales_actual' => $esEntity->getSalesActual(),
            'chip' => $esEntity->getChip(),
            'status' => $esEntity->getStatus(),
            'c_id' => $esEntity->getCId(),
            'pre_today_money' => $esEntity->getPreTodayMoney(),
            'pre_today_money_date' => $esEntity->getPreTodayMoneyDate(),
        ];
        return $params;
    }
}