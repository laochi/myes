<?php
namespace app\services\ElasticSearch\Requests\goods;

use app\services\ElasticSearch\RequestAbstract;
use app\services\ElasticSearch\RequestInterface;

class IndexDelete extends RequestAbstract implements RequestInterface
{
    public $tpl = [
        'index' => '',
    ];

    public function assignTpl(array $params = [])
    {
        $indexName = $params['index_name'] ?? '';
        $this->tpl['index'] = $indexName;
    }

    public function handleResponse(array $response = [])
    {
        $responseData = [
            'success' => false,
        ];
        if (isset($response['acknowledged']) && $response['acknowledged']) {
            $responseData['success'] = true;
        }
        return $responseData;
    }

    public function sendRequest()
    {
        return $this->indexDelete($this->getTplArr());
    }

    public function hasParamError(array $params = [])
    {
        if (! isset($params['index_name']) || '' == trim($params['index_name'])) {
            $this->errorMsg = 'EmptyIndexName';
            return true;
        }
        return false;
    }
}