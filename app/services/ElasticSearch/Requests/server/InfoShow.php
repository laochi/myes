<?php
namespace app\services\ElasticSearch\Requests\server;

use app\services\ElasticSearch\RequestAbstract;
use app\services\ElasticSearch\RequestInterface;
use app\services\ElasticSearch\Helper as EsHelper;

class InfoShow extends RequestAbstract implements RequestInterface
{
    public $tpl = [];

    public function assignTpl(array $params = []){}

    public function handleResponse(array $response = [])
    {
        $responseData = [];
        if (isset($response['name']) && $serverName = $response['name']) {
            $responseData['server_name'] = $serverName;
        }
        if (isset($response['cluster_name']) && $serverName = $response['cluster_name']) {
            $responseData['cluster_name'] = $serverName;
        }
        if (isset($response['cluster_uuid']) && $serverName = $response['cluster_uuid']) {
            $responseData['cluster_uuid'] = $serverName;
        }
        if (isset($response['version']) && $version = $response['version']) {
            if (isset($version['number']) && $versionNumber = $version['number']) {
                $responseData['version_number'] = $versionNumber;
            }
        }
        return $responseData;
    }

    public function sendRequest()
    {
        return $this->serverInfo($this->getTplArr());
    }

    public function hasParamError(array $params = [])
    {
        return false;
    }
}