<?php
namespace app\services\ElasticSearch\Requests\server;

use app\services\ElasticSearch\RequestAbstract;
use app\services\ElasticSearch\RequestInterface;
use app\services\ElasticSearch\Helper as EsHelper;

class IndexExists extends RequestAbstract implements RequestInterface
{
    public $tpl = [
        'index' => '',
    ];

    public function assignTpl(array $params = [])
    {
        $indexName = $params['index_name'] ?? '';
        $this->tpl['index'] = $indexName;
    }

    public function handleResponse(array $response = [])
    {
        $responseData = [];
        if (isset($response['exists'])) {
            $responseData['exists'] = $response['exists'];
        }
        return $responseData;
    }

    public function sendRequest()
    {
        return $this->indexExists($this->getTplArr());
    }

    public function hasParamError(array $params = [])
    {
        if (! isset($params['index_name']) || '' == trim($params['index_name'])) {
            $this->errorMsg = 'EmptyIndexName';
            return true;
        }
        return false;
    }
}